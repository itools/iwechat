<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace iWechat\msg;

use iWechat\msg\HttpMsgContext;

/**
 * Description of HttpMsgLogModule
 *
 * @author chenxy <chenxy@mysoft.com.cn>
 */
class HttpMsgLogModule implements \iWechat\interfaces\IHttpMsgModule
{
    public function init(MessageServer $app)
    {
        $app->beforeExecHandleEvents[__CLASS__] = 'before_handle_event';
    }
    
    public static function before_handle_event(HttpMsgContext $context, $args)
    {
        $appid = $context->request->getQueryParam('appid', '');
        $requestTime = date("Y-m-d H:i:s", $_SERVER['REQUEST_TIME']);
        $xml = $context->request->requestXml;
        $data = $context->request->requestData;
        $fromUser = array_key_exists('FromUserName', $data) ? $data['FromUserName'] : '';
        $toUser = array_key_exists('ToUserName', $data) ? $data['ToUserName'] : '';
        $msgTimestamp = array_key_exists('CreateTime', $data) ? intval($data['CreateTime']) : 0;
        $oMsgType = array_key_exists('MsgType', $data) ? $data['MsgType'] : '';
        // 全网发布用full-web-publishing，开放平台推送用component,未识别的用unknown
        $msgType = $context->request->isFullWebPublishing
                ? "publish"
                : ($oMsgType?:(array_key_exists('InfoType', $data) ? 'ticket' : 'unknown'));
        
        // 微信开发平台通知
        if ($msgType == 'ticket') {
            $fromUser = array_key_exists('AuthorizerAppid', $data) ? $data['AuthorizerAppid'] : '';
            $toUser = array_key_exists('Appid', $data) ? $data['Appid'] : '';
            $appid = $toUser;
        }
        
        $row = [
            'appid' => $appid,
            'from_user' => $fromUser,
            'to_user' => $toUser,
            'called_url' => $_SERVER['REQUEST_URI'],
            'receive_time' => $requestTime,
            'msg_timestamp' => $msgTimestamp,
            'msg_type' => $msgType,
            'original_xml' => $xml
        ];

        \Yii::$app->getDb()->createCommand()->insert('wx_msg', $row)->execute();
    }
}
