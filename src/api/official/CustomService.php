<?php

namespace iWechat\api\official;

/**
 * 多客服信息
 */
use iWechat\api\ApiBase;

class CustomService extends ApiBase
{
    /**
     * 获取多客服基本信息列表
     * @return object
     */
    public function getKfList()
    {
        $params =[];
        $info = $this->vget('https://api.weixin.qq.com/cgi-bin/customservice/getkflist', "客服信息列表", $params);
        return $info;
    }

    /**
     * 获取多客服在线情况
     * @return object
     */
    public function getOnlineKfList()
    {
        $params =[];
        $info = $this->vget('https://api.weixin.qq.com/cgi-bin/customservice/getonlinekflist', "客服在线情况", $params);
        return $info;
    }
}
