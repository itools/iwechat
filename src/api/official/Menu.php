<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace iWechat\api\official;

use iWechat\api\ApiBase;

/**
 * 自定义菜单相关接口
 *
 * @author Chenxy
 */

class Menu extends ApiBase
{
    /**
     * 创建自定义菜单
     * @param array $buttons,可通过iWechat\helper\ButtonFactory创建
     * @return object {"errcode":0,"errmsg":"ok"}
     */
    public function create($buttons)
    {
        $params =['button' => $buttons];
        $result = $this->vpost('https://api.weixin.qq.com/cgi-bin/menu/create', '创建自定义菜单', $params);
        return $result;
    }
    
    /**
     * 查询自定义菜单
     * @return type
     */
    public function get()
    {
        $params =[];
        $buttons = $this->vget('https://api.weixin.qq.com/cgi-bin/menu/get', '查询自定义菜单', $params);
        return $buttons;
    }
    
    /**
     * 删除自定义菜单
     * @return type
     */
    public function delete()
    {
        $params =[];
        $buttons = $this->vget('https://api.weixin.qq.com/cgi-bin/menu/delete', '删除自定义菜单', $params);
        return $buttons;
    }
    
    /**
     * 获取自定义菜单配置
     * @return type
     */
    public function getCurrentSelfmenuInfo()
    {
        $params =[];
        $buttons = $this->vget('https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info', '获取自定义菜单配置', $params);
        return $buttons;
    }
}
