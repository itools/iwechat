<?php

namespace iWechat\api;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiBase
 *
 * @author Chenxy
 */
use iWechat\interfaces\IAccessTokenHelper;
use iWechat\exceptions\WechatException;

class ApiBase
{
    /**
     * AccessTokenHelper
     * @var IAccessTokenHelper
     */
    protected $_accessTokenHelper;
    
    /**
     * 设置执行次数，最小值为1，重试次数=$_maxInvokeTimes -1
     * @var int
     */
    protected $_maxInvokeTimes = 1;
    
    public function __construct(IAccessTokenHelper $accessTokenHelper)
    {
        $this->_accessTokenHelper = $accessTokenHelper;
    }
    
    /**
     * 设置调用接口次数
     * @param int $invokeTimes
     * @throws \InvalidArgumentException
     */
    public function setMaxInvokeTimes(int $invokeTimes)
    {
        if ($invokeTimes < 1) {
            throw new \InvalidArgumentException('必须为大于0的整数');
        }
        
        $this->_maxInvokeTimes = $invokeTimes;
    }
    
    /**
     * post方法的微信接口
     * @param string $uri 请求的uri地址
     * @param string $description 描述
     * @param array $data 请求参数
     * @param bool $accessToken
     * @param bool $jsonParse
     * @return mixed string or object
     */
    public function vpost($uri, $description, $data = [], $accessToken = true, $jsonParse = true)
    {
        return $this->internalInvoke($uri, 'POST', $description, $data, $accessToken, $jsonParse);
    }
    
    /**
     * get方法的微信接口
     * @param string $uri 请求的uri地址
     * @param string $description 描述
     * @param array $data 请求参数
     * @param bool $accessToken
     * @param bool $jsonParse
     * @return mixed string or object
     */
    public function vget($uri, $description, $data = [], $accessToken = true, $jsonParse = true)
    {
        return $this->internalInvoke($uri, 'GET', $description, $data, $accessToken, $jsonParse);
    }

    /**
     * 调用接口
     * @param string $uri 接口URL
     * @param string $method 'GET\POST' HTTP方法
     * @param string $apiDescription 接口描述
     * @param array $data 接口数据 get方法时可作为url参数数据， post方法时作为传输的数据
     * @param bool $autoAppendAccessTokenParam 是否url自动带入accesstoken参数
     * @param bool $jsonParse 是否对返回结果进行json反序列化，默认true
     * @return object 微信接口返回结果
     * @throws \InvalidArgumentException
     * @throws iWechat\exceptions\WechatException
     */
    private function internalInvoke($uri, $method, $apiDescription, $data = [], $autoAppendAccessTokenParam = true, $jsonParse = true)
    {
        if (!in_array($method, ['GET','POST'])) {
            throw new \InvalidArgumentException('参数值只能为GET、POST');
        }
        
        if (strpos(strtolower(json_encode($data)), "access_token") !== false) {
            throw new WechatException("{$apiDescription}失败，数据包中含有access_token字符串，请求已被拦截 数据包内容:" . json_encode($data, JSON_UNESCAPED_UNICODE));
        }
        
        $occurError = false;
        $accessTokenExpiredTryTimes = 0;
        $invokeUri = $autoAppendAccessTokenParam ? $this->getWithAccessToken($uri) : $uri;
        $restClient = (new \linslin\yii2\curl\Curl())->setOption(CURLOPT_HTTPHEADER, ['Accept' => 'application/json'])->setOption(CURLOPT_SSL_VERIFYPEER, false);
        if ($method == 'POST' && $data) {
            $restClient->setOption(CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));
        }
        // 调用微信接口
        for ($i = 0; $i < $this->_maxInvokeTimes; $i++) {
            $result = ($method == 'POST')
                    ? $restClient->post($invokeUri, true)
                    : $restClient->get($invokeUri . '&' . http_build_query($data), true);
            // 返回成功
            if (strpos($result, '"errcode":') === false || strpos($result, '"errcode":0') !== false) {
                    $occurError = false;
                    break;
            }
            // 失败重试
            $occurError = true;
            $errorResult = json_decode($result);
            $this->_accessTokenHelper->makeExpire($errorResult->errcode);
            // access_token过期自动重试3次
            if ($autoAppendAccessTokenParam && $this->_accessTokenHelper->checkIsExpired($errorResult->errcode) && $accessTokenExpiredTryTimes < 3) {
                $i--;
                $accessTokenExpiredTryTimes++;
                $invokeUri = $this->getWithAccessToken($uri);
            }
        }
        
        if ($occurError) {
            $msg = "调用[$apiDescription]失败，错误码:[$errorResult->errcode] 消息:[$errorResult->errmsg] 参数:"
                    . (count($data) == 0 ? '无':  json_encode($data, JSON_UNESCAPED_UNICODE))
                    . "] 接口url:[{$invokeUri}]";
            
            if ($this->checkIsWarning($errorResult->errcode)) {
                \Yii::warning($msg);
            } else {
                throw new WechatException($msg, $errorResult->errcode);
            }
        }
        
        return $jsonParse ? json_decode($result) : $result;
    }
    
    private function checkIsWarning($errcode)
    {
        return in_array($errcode, [
            43004, // 未关注或者客服消息接口48小时内未发生交互
            40003, // 不合法的OpenID
            43004, // 需要接收者关注
        ]);
    }
    
    protected function getWithAccessToken($uri)
    {
        $accessTokenParamName = $this->_accessTokenHelper->getAccessTokenParamName();
        $accessToken = $this->_accessTokenHelper->getAccessToken();
        $uri .= strpos($uri, '?') === false ? '?' : '&';
        $uri .= "{$accessTokenParamName}={$accessToken}";
        return $uri;
    }
}
