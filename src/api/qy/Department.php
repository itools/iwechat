<?php

namespace iWechat\api\qy;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 企业微信部门类接口
 *
 * @author Chenxy
 */
use iWechat\interfaces\IAccessTokenHelper;
use iWechat\api\ApiBase;

class Department extends ApiBase
{
    public function __construct(IAccessTokenHelper $accessTokenHelper)
    {
        parent::__construct($accessTokenHelper);
    }

    /**
     * 获取指定部门及其下的子部门
     * @param string $id
     * @return object {
        "errcode": 0,
        "errmsg": "ok",
        "department": [
            {
                "id": 2,
                "name": "广州研发中心",
                "parentid": 1,
                "order": 10
            },
            {
                "id": 3
                "name": "邮箱产品部",
                "parentid": 2,
                "order": 40
            }
        ]
    }
     */
    public function getDepartmentInfo($id)
    {
        $params =['id' => $id];
        return $this->vget('https://qyapi.weixin.qq.com/cgi-bin/department/list', '通过id获取部门及其下的子部门·', $params);
    }
}
