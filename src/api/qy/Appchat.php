<?php

namespace iWechat\api\qy;

use iWechat\interfaces\IAccessTokenHelper;
use iWechat\api\ApiBase;

/**
 * 群聊接口
 *
 * @author ray
 */
class Appchat extends ApiBase
{
    public function __construct(IAccessTokenHelper $accessTokenHelper)
    {
        parent::__construct($accessTokenHelper);
    }
    
    /**
     * 创建群聊
     * @param string $name 群名称
     * @param string $owner 群主,可选
     * @param array $userlist 群成员,至少两名
     * @param string $chatid 群id,可选
     * @return  {
                "errcode" : 0,
                "errmsg" : "ok",
                "chatid" : "CHATID"
              }
     */
    public function create($name, $userlist, $owner = '', $chatid = '')
    {
        if (!is_array($userlist) || count($userlist) < 2) {
            throw new \iWechat\exceptions\ApiParamException("参数userlist不是数组或元素少于两个");
        }
        
        $data =['name' => $name, 'userlist' => $userlist];
        if ($owner) {
            $data['owner'] = $owner;
        }
        
        if ($chatid) {
            $data['$chatid'] = $chatid;
        }
        
        $result = $this->vpost('https://qyapi.weixin.qq.com/cgi-bin/appchat/create', '创建群聊', $data);
        return $result;
    }
    
    /**
     * 发送文本消息
     * @param string $chatid
     * @param string $content
     * @param int $safe
     * @return array  {
                    "errcode" : 0,
                    "errmsg" : "ok",
                  }
     */
    public function sendText($chatid, $content, $safe = 0)
    {
        $data = ['chatid' => $chatid, 'msgtype' => 'text', 'safe' => $safe];
        $data['text'] = ['content' => $content];
        $sendResult = $this->vpost('https://qyapi.weixin.qq.com/cgi-bin/appchat/send', '发送群聊文本消息', $data);
        return $sendResult;
    }
    
    /**
     * 发送markdown消息
     * @param string $chatid
     * @param string $content
     * @return array  {
                    "errcode" : 0,
                    "errmsg" : "ok",
                  }
     */
    public function sendMarkdown($chatid, $content)
    {
        $msgContent = ['content' => $content];
        $data =['chatid' => $chatid, 'msgtype' => 'markdown', 'markdown' => $msgContent];
        $sendResult = $this->vpost('https://qyapi.weixin.qq.com/cgi-bin/appchat/send', '发送群聊markdown消息', $data);
        return $sendResult;
    }
    
    /**
     * 发送卡片消息
     * @param string $chatid
     * @param string $title
     * @param string $description
     * @param string $url
     * @param string $btntxt
     * @param int $safe
     * @return array  {
                    "errcode" : 0,
                    "errmsg" : "ok",
                  }
     */
    public function sendTextCard($chatid, $title, $description, $url, $btntxt = '详情', $safe = 0)
    {
        $textCard = ['title' => $title, 'description' => $description, 'url' => $url, 'btntxt' => $btntxt, 'safe' => $safe];
        $data = ['chatid' => $chatid, 'msgtype' => 'textcard', 'textcard' => $textCard];
        $sendResult = $this->vpost('https://qyapi.weixin.qq.com/cgi-bin/appchat/send', '发送群聊文本卡片消息', $data);
        return $sendResult;
    }
}
