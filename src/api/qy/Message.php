<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace iWechat\api\qy;

/**
 * Description of Message
 *
 * @author Chenxy
 */
use iWechat\interfaces\IAccessTokenHelper;
use iWechat\api\ApiBase;

class Message extends ApiBase
{
    public function __construct(IAccessTokenHelper $accessTokenHelper)
    {
        parent::__construct($accessTokenHelper);
    }
    
    /**
     * 发送普通文本消息
     * @param string $touser 成员id列表，多个用|分隔
     * @param string $content 消息内容
     * @param int $agentid 企业应用的id
     * @return object {
                    "errcode": 0,
                    "errmsg": "ok",
                    "invaliduser": "UserID1",
                    "invalidparty":"PartyID1",
                    "invalidtag":"TagID1"
                 }
     */
    public function sendText($touser, $content, $agentid)
    {
        $msgContent = ['content' => $content];
        $data =['touser' => $touser, 'msgtype' => 'text', 'agentid' => $agentid, 'text' => $msgContent];
        $sendResult = $this->vpost('https://qyapi.weixin.qq.com/cgi-bin/message/send', '发送文本消息', $data);
        return $sendResult;
    }
    
    /**
     * 发送markdown消息
     * @param string $touser
     * @param string $content
     * @param int $agentid
     * @return object {
                    "errcode": 0,
                    "errmsg": "ok",
                    "invaliduser": "UserID1",
                    "invalidparty":"PartyID1",
                    "invalidtag":"TagID1"
                 } 
     */
    public function sendMarkdown($touser, $content, $agentid)
    {
        $msgContent = ['content' => $content];
        $data =['touser' => $touser, 'msgtype' => 'markdown', 'agentid' => $agentid, 'markdown' => $msgContent];
        $sendResult = $this->vpost('https://qyapi.weixin.qq.com/cgi-bin/message/send', '发送markdown消息', $data);
        return $sendResult;
    }
    
    /**
     * 发送卡片消息
     * @param string $touser
     * @param string $title
     * @param string $description
     * @param string $url
     * @param string $agentid
     * @param string $btntxt
     * @return object {
                    "errcode": 0,
                    "errmsg": "ok",
                    "invaliduser": "UserID1",
                    "invalidparty":"PartyID1",
                    "invalidtag":"TagID1"
                 } 
     */
    public function sendTextCard($touser, $title, $description, $url, $agentid, $btntxt = '详情')
    {
        $textCard = ['title' => $title, 'description' => $description, 'url' => $url, 'btntxt' => $btntxt];
        $data = ['touser' => $touser, 'msgtype' => 'textcard', 'agentid' => $agentid, 'textcard' => $textCard];
        $sendResult = $this->vpost('https://qyapi.weixin.qq.com/cgi-bin/message/send', '发送文本卡片消息', $data);
        return $sendResult;
    }
}
