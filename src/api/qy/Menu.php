<?php

/**
 * Created by wangl10@mysoft.com.cn.
 * Date: 2015/6/13
 * Time: 15:09
 */

namespace iWechat\api\qy;

use iWechat\interfaces\IAccessTokenHelper;
use iWechat\api\ApiBase;

class Menu extends ApiBase
{
    public function __construct(IAccessTokenHelper $accessTokenHelper)
    {
        parent::__construct($accessTokenHelper);
    }

    /**
     * 创建自定义菜单
     * @param $buttons
     * @param $agentId
     * @return object
     * @throws \app\framework\weixin\WeixinException
     */
    public function create($buttons, $agentId)
    {
        $params = ['button' => $buttons];
        $result = $this->vpost(('https://qyapi.weixin.qq.com/cgi-bin/menu/create?agentid=' . $agentId), '创建自定义菜单', $params);
        return $result;
    }

    /**
     * 删除自定义菜单
     * @param $agentId
     * @return object
     * @throws \app\framework\weixin\WeixinException
     */
    public function delete($agentId)
    {
        $params = [];
        $result = $this->vget(('https://qyapi.weixin.qq.com/cgi-bin/menu/delete?agentid=' . $agentId), '删除自定义菜单', $params);
        return $result;
    }

    /**
     * 查询自定义菜单
     * @param $agentId
     * @return object
     * @throws \app\framework\weixin\WeixinException
     */
    public function get($agentId)
    {
        $params = [];
        $buttons = $this->vget(('https://qyapi.weixin.qq.com/cgi-bin/menu/get?agentid=' . $agentId), '查询自定义菜单', $params);
        return $buttons;
    }
}
