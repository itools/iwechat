<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace iWechat\token;

use iWechat\interfaces\IPlatformAccessTokenRepository;
use iWechat\exceptions\WechatException;

/**
 * 微信第三方开放平台PlatformAccessTokenRepository仓储
 *
 * @author chenxy
 */
class PlatformAccessTokenRepository implements IPlatformAccessTokenRepository
{
    /**
     * 获取wxInvoker
     * @param string $appId 第三方平台appid
     * @return \iWechat\api\WxInvoker
     */
    public function getWxInvoker($appId)
    {
        // 从数据缓存中读取wxInvoke数据
        $cache = \Yii::$app->cache;
        $cacheKey = $this->getWxInvokerCacheKey($appId);
        if ($cache->exists($cacheKey)) {
            return $cache[$cacheKey];
        }
        
        // 从数据库表中构造
        return $this->getWxInvokerByConfig($appId);
    }
    
    public function updateAccessToken($appId, $accessToken, $expireTime)
    {
         // 更新到数据缓存
        $wxInvoker = $this->getWxInvoker($appId);
        $wxInvoker->accessToken = $accessToken;
        $wxInvoker->expireTime = $expireTime;
        $cache = \Yii::$app->cache;
        $cacheKey = $this->getWxInvokerCacheKey($appId);
        $cache[$cacheKey] = $wxInvoker;
    }
    
    public function getConfigValue($appId, $configKey)
    {
        $wxConfig = $this->getSecretConfig($appId);
        if (!array_key_exists($configKey, $wxConfig)) {
            throw new WeixinException("找不到配置项{$configKey}");
        }
        
        return $wxConfig[$configKey];
    }
    
    public function updateVerifyTicket($appId, $ticket)
    {
        $cacheKey = $this->getTicketCacheKey($appId);
        $cache = \Yii::$app->cache;
        $cache[$cacheKey] = $ticket;
        $this->updateConfigValue($appId, "ticket", $ticket);
    }
    
    public function getVerifyTicket($appId)
    {
        $cacheKey = $this->getTicketCacheKey($appId);
        $cache = \Yii::$app->cache;
        if ($cache->exists($cacheKey)) {
            return $cache[$cacheKey];
        }
        return $this->getConfigValue($appId, "ticket");
    }

    public function updateConfigValue($appId, $configKey, $configValue)
    {
        $config = $this->getSecretConfig($appId);
        $config[$configKey] = $configValue;
        $newConfig = json_encode($config, JSON_UNESCAPED_UNICODE);
        $key = $this->getConfigCacheKey($appId);
        \yii::$app->getDb()->createCommand()->update('wx_config', ['value' => $newConfig], ['name' => $key])->execute();
        \Yii::$app->cache->set($key, $newConfig, 86400);
    }
    
    private function getWxInvokerByConfig($appId)
    {
        $wxInvokeConfig =$this->getSecretConfig($appId);
        $wxInvoker = new \iWechat\api\WxInvoker();
        $wxInvoker->appId = $wxInvokeConfig['app_id'];
        //$wxInvoker->appSecret = $wxInvokeConfig['app_secret'];
        $wxInvoker->appType = "开放平台";
        $wxInvoker->token = $wxInvokeConfig["token"];
        $wxInvoker->encodingKey = $wxInvokeConfig["encoding_key"];
        return $wxInvoker;
    }
    
    private function getSecretConfig($appId)
    {
        // 优先从缓存取
        $cache = \Yii::$app->cache;
        $key = $this->getConfigCacheKey($appId);
        if ($cache->exists($key) && $cache->get($key)) {
            return json_decode($cache->get($key),true);
        }
        
        // 从数据库中读取并缓存
        $config = (new \yii\db\Query())->from('wx_config')->select("value")->where(['name' => $key])->createCommand()->queryScalar();
        if ($config) {
            $cache->set($key, $config, 86400);
            $config = json_decode($config, true);
            return $config;
        }
        
        if ($config === false) {
            throw new WechatException("缺少微信第三方开放平台配置{$key}");
        }
    }
    
    private function getWxInvokerCacheKey($appId)
    {
        return "wechat_platform_invoker_$appId";
    }
    
    private function getConfigCacheKey($appId)
    {
        return $appId;
        //return "wechat_platform_config_$appId";
    }
    
    private function getTicketCacheKey($appId)
    {
        return "wechat_platform_ticket_$appId";
    }

    public static function clearCache($appId)
    {
        $cache = \Yii::$app->cache;
        $self = new self();
        $cacheKey = $self->getWxInvokerCacheKey($appId);
        $cache->exists($cacheKey) && $cache->delete($cacheKey);
        $cacheKey = $self->getTicketCacheKey($appId);
        $cache->exists($cacheKey) && $cache->delete($cacheKey);
        $cacheKey = $self->getConfigCacheKey($appId);
        $cache->exists($cacheKey) && $cache->delete($cacheKey);
    }
}
