<?php

namespace iWechat\token;

/*
 * 获取accesstoken帮助类，支持企业号、服务号、订阅号，其中对于服务号和订阅号支持开发模式和授权模式
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use iWechat\interfaces\IAccessTokenRepository;
use iWechat\interfaces\IAccessTokenHelper;
use iWechat\exceptions\WechatException;

/**
 * Description of AccessTokenHelper
 *
 * @author Chenxy
 */
class AccessTokenHelper implements IAccessTokenHelper
{
    /**
     * accessTokenRepository
     * @var \iWechat\interfaces\IAccessTokenRepository
     */
    public $accessTokenRepository;
    
    /**
     * _wxInvoker
     * @var \iWechat\api\WxInvoker
     */
    protected $_wxInvoker;
    protected $_id;
    private $_isAuthAccess;
    
    /**
     * 构造方法
     * @param string $id 公众号对应的唯一标识符,支持account_id和original_id
     * @param \iWechat\interfaces\IAccessTokenRepository $repository
     */
    public function __construct($id, IAccessTokenRepository $repository = null)
    {
        $this->_id = $id;
        // 不采用策略模式，省得各个站点都要去配置注入
        // $this->accessTokenRepository = $repository ?: \Yii::$container->get('iWechat\interfaces\IAccessTokenRepository');
        $this->accessTokenRepository = $repository ?: (new AccessTokenRepository());
        $this->_isAuthAccess = ($this->accessTokenRepository->getConfigValue($id, 'is_authed') == 1);
        // 通过仓储获取当前调用接口的信息
        $this->_wxInvoker = $this->accessTokenRepository->getWxInvoker($id);
        // 增强健壮性
        if ($this->_isAuthAccess && empty($this->_wxInvoker->authRefreshToken)) {
            $authRefreshToken = $this->accessTokenRepository->getConfigValue($id, 'authorizer_refresh_token');
            $this->_wxInvoker->authRefreshToken = $authRefreshToken;
        }
    }
    
    /**
     * 获取access_token在url的参数名
     * @return string
     */
    public function getAccessTokenParamName()
    {
        // 官方文档说明（只是需将调用API时提供的公众号自身access_token参数，替换为authorizer_access_token）但实际不是
        return "access_token";
        //return $this->_isAuthAccess ? "authorizer_access_token" : "access_token";
    }
        
    /**
     * 获取access_token
     * @return string
     */
    public function getAccessToken()
    {
        if ($this->isExpire()) {
            $this->freshAccessToken();
        }
        return $this->_wxInvoker->accessToken;
    }
    
    /**
     * 设置access_token过期,无参时强制过期
     * @param int $errorCode
     */
    public function makeExpire($errorCode = -1)
    {
        if ($errorCode == -1 || $this->checkIsExpired($errorCode)) {
            $this->_wxInvoker->accessToken = '';
            $this->_wxInvoker->expireTime = 0;
            $this->accessTokenRepository->updateAccessToken($this->_id, '', null);
        }
    }
    
    /**
     * 通过接口获取access_token
     * @throws WeixinException
     */
//    protected function freshAccessToken()
    //这里为了兼容旧的JSSDK中regetAccessToken公开出来
    public function freshAccessToken()
    {
        $result = $this->_isAuthAccess
                ? $this->invokeAccesTokenByWechatAuth()
                : $this->invokeAccessTokenByAppSecert();

        // 获取accesstoken失败
        if ($result->errcode) {
            throw new WechatException('获取access_token失败，错误码：' . $result->errcode . '消息：' . $result->errmsg);
        }
        
        $accessToken = $this->_isAuthAccess ? $result->authorizer_access_token : $result->access_token;
        $authRefreshToken = $this->_isAuthAccess ? $result->authorizer_refresh_token : "";
        // 有效期－60
        $expireTime = time() + intval($result->expires_in) - 60;
        $this->_wxInvoker->accessToken = $accessToken;
        $this->_wxInvoker->expireTime = $expireTime;
        $this->_wxInvoker->authRefreshToken = $authRefreshToken;
        
        // 更新
        $this->accessTokenRepository->updateAccessToken($this->_id, $accessToken, $expireTime, $authRefreshToken);
    }
    
    /**
     * 判断access_token是否已过期
     * @return bool
     */
    protected function isExpire()
    {
        return empty($this->_wxInvoker)
                || empty($this->_wxInvoker->accessToken)
                || empty($this->_wxInvoker->expireTime)
                || time() >= $this->_wxInvoker->expireTime;
    }
    
    private function invokeAccesTokenByWechatAuth()
    {
        $api = new \iWechat\api\platform\WxComponent();
        $appId = $this->_wxInvoker->appId;
        $result = $api->getAuthorizerToken($appId, $this->_wxInvoker->authRefreshToken);
        return $result;
    }
    
    private function invokeAccessTokenByAppSecert()
    {
        $apiUrl = $this->_wxInvoker->buildGetTokenUrl();
        $result = (new \linslin\yii2\curl\Curl())->setOption(CURLOPT_HTTPHEADER, ['Accept' => 'application/json'])->setOption(CURLOPT_SSL_VERIFYPEER, false)->get($apiUrl, true);
        return json_decode($result);
    }
    
    /**
     * 根据错误码判断access_token是否过期
     * @param int $errorCode
     * @return bool
     */
    public function checkIsExpired($errorCode)
    {
        return ($errorCode == 42001 || $errorCode == 40001 || $errorCode == 40014);
    }
    
    /**
     * 获取唯一标识
     * @return string
     */
    public function getId()
    {
        // 保证兼容构建函数各种id
        // return $this->accessTokenRepository->getConfigValue($this->_id, 'id');
        // 为了兼容旧的代码使用token(\framework\weixin\JSSDK.php)
        return $this->accessTokenRepository->getConfigValue($this->_id, 'token');
    }
    
    /**
     * 获取微信appid
     * @return string
     */
    public function getAppId()
    {
        if ($this->_wxInvoker && $this->_wxInvoker->appId) {
            return $this->_wxInvoker->appId;
        }
        
        // 从配置中读取
        return $this->accessTokenRepository->getConfigValue($this->_id, 'app_id');
    }
}
