<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace iWechat\token;

use iWechat\interfaces\IPlatformAccessTokenRepository;
use iWechat\api\platform\WxComponent;
use iWechat\exceptions\WechatException;

/**
 * Description of PlatformAccessTokenHelper
 *
 * @author chenxy
 */
class PlatformAccessTokenHelper extends AccessTokenHelper
{
    const API_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/component/api_component_token";
    
    /**
     * 构造方法
     * @param string $platformAppId 微信开放平台appid
     * @param IPlatformAccessTokenRepository $repository
     */
    public function __construct($platformAppId, IPlatformAccessTokenRepository $repository = null)
    {
        // 不采用策略模式，省得各个站点都要去配置注入
//        $this->accessTokenRepository = $repository ?: \Yii::$container->get('iWechat\interfaces\IPlatformAccessTokenRepository');
        $this->accessTokenRepository = $repository ?: (new PlatformAccessTokenRepository());
        $this->_id = $platformAppId;
        // 通过仓储获取当前调用接口的信息
        $this->_wxInvoker = $this->accessTokenRepository->getWxInvoker($platformAppId);
    }

    public function getAccessTokenParamName()
    {
        return "component_access_token";
    }

    public function freshAccessToken()
    {
        // 获取企业开发者访问api所需数据
        $appId = WxComponent::getComponentAppId();
        $ticket = $this->accessTokenRepository->getVerifyTicket($appId);
        $params = ["component_appid" => $this->_wxInvoker->appId ,
                    "component_appsecret" => $this->_wxInvoker->appSecret,
                    "component_verify_ticket" => $ticket ];
        $result = (new \linslin\yii2\curl\Curl())->setOption(CURLOPT_HTTPHEADER, ['Accept' => 'application/json'])
                ->setOption(CURLOPT_SSL_VERIFYPEER, false)
                ->setOption(CURLOPT_POSTFIELDS, json_encode($params, JSON_UNESCAPED_UNICODE))
                ->post(self::API_ACCESS_TOKEN_URL, true);
        
        $result = json_decode($result);
        
        // 获取accesstoken失败
        if (isset($result->errcode)) {
            throw new WechatException('获取component_access_token失败，错误码：' . $result->errcode . '消息：' . $result->errmsg);
        }
        
        $accessToken = $result->component_access_token;
        
        // 设定过期时间
        $expireTime = time() + intval($result->expires_in) - 60;
        $this->_wxInvoker->accessToken = $accessToken;
        $this->_wxInvoker->expireTime = $expireTime;
        
        // 更新
        $this->accessTokenRepository->updateAccessToken($this->_id, $accessToken, $expireTime);
    }
    
    public function getId()
    {
        return $this->_id;
    }
    
    public function getAppId()
    {
        return $this->accessTokenRepository->getConfigValue($this->_id, 'app_id');
    }
}
