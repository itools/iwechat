# iWechat
微信开发SDK,包括企业号,服务号,消息推送,支持开发者和托管模式。

### 安装方法
------------
```
composer require mingyuanyun/iwechat
```